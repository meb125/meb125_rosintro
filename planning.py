#!/usr/bin/env python3

#imports
from __future__ import print_function
from six.moves import input

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

import math

#initializing the moveit and rospy objects
moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node("midtermProject", anonymous=True)

#initializing the RobotCommander, scene infterface, and MoveGroupCommander
robot = moveit_commander.RobotCommander()
scene = moveit_commander.PlanningSceneInterface()
move_group = moveit_commander.MoveGroupCommander("manipulator")

#initializing the ROS publisher to display the trajectories
display_trajectory_publisher = rospy.Publisher(
    "/move_group/display_planned_path",
    moveit_msgs.msg.DisplayTrajectory,
    queue_size=20,
)

#Helper function that plans and executes a movement
def planAndExecute():
    plan = move_group.go(wait=True)
    move_group.stop()
    move_group.clear_pose_targets()

#Function that traces the M
def drawM():
    #Draws the left vertical line
    for i in range(20):
        pose_goal.position.x = 0.7
        pose_goal.position.y = 0.0
        pose_goal.position.z = (0.025*i + 0.1)
        move_group.set_pose_target(pose_goal)

    planAndExecute()

    #Draws the left downward diagonal line
    for i in range(8):
        pose_goal.position.y = (0.025*i)
        pose_goal.position.z = (0.575 - 0.025*i)
        move_group.set_pose_target(pose_goal)

    planAndExecute()

    #Draws the right upward diagonal line
    for i in range(8):
        pose_goal.position.y = (0.175 + 0.025*i)
        pose_goal.position.z = (0.4 + 0.025*i)
        move_group.set_pose_target(pose_goal)

    planAndExecute()

    #Draws the right vertical line
    for i in range(20):
        pose_goal.position.z = (0.575 - 0.025*i)
        move_group.set_pose_target(pose_goal)

    planAndExecute()

#Function that traces the E
def drawE():
    #Sets the intial pose to be the top right point of the E
    for i in range(20):
        pose_goal.position.z = 0.025*i + 0.1
        move_group.set_pose_target(pose_goal)

    planAndExecute()

    #Draws the top horizontal line
    for i in range(15):
        pose_goal.position.y = (0.35 - 0.025*i)
        move_group.set_pose_target(pose_goal)
    
    planAndExecute()

    #Draws the top half of the left vertical line, down to the middle horizontal line
    for i in range(11):
        pose_goal.position.z = (0.575 - 0.025*i)
        move_group.set_pose_target(pose_goal)
    
    planAndExecute()

    #Draws the middle horizontal line
    for i in range(8):
        pose_goal.position.y = 0.025*i
        move_group.set_pose_target(pose_goal)
    
    planAndExecute()

    #Returns back to the left side of the E to continue
    for i in range(8):
        pose_goal.position.y = (0.175 - 0.025*i)
        move_group.set_pose_target(pose_goal)
    
    planAndExecute()

    #Draws the bottom half of the left vertical line, down from the middle horizontal line to the base
    for i in range(10):
        pose_goal.position.z = (0.325 - 0.025*i)
        move_group.set_pose_target(pose_goal)
    
    planAndExecute()

    #Draws the bottom horizontal line
    for i in range(15):
        pose_goal.position.y = 0.025*i
        move_group.set_pose_target(pose_goal)
    
    planAndExecute()

    #Returns back to the left side to continue tracing the next letter
    for i in range(15):
        pose_goal.position.y = (0.35 - 0.025*i)
        move_group.set_pose_target(pose_goal)

    planAndExecute()

#Function that traces the B
def drawB():
    #Draws the left vertical line of the B
    for i in range(20):
        pose_goal.position.x = 0.7
        pose_goal.position.y = 0.0
        pose_goal.position.z = (0.025*i + 0.1)
        move_group.set_pose_target(pose_goal)

    planAndExecute()

    #Draws the top curve of the B
    for i in range(41):
        pose_goal.position.z = (0.575 - .00625*i)
        pose_goal.position.y = math.sqrt(0.125**2 - (pose_goal.position.z - 0.45)**2 + 0.0000001)
        move_group.set_pose_target(pose_goal)
        planAndExecute()

    #Draws the bottom curve of the B
    for i in range(37):
        pose_goal.position.z = (0.325 - .00625*i)
        pose_goal.position.y = math.sqrt(0.1125**2 - (pose_goal.position.z - 0.2125)**2 + 0.0000001)
        move_group.set_pose_target(pose_goal)
        planAndExecute()

#initializing the pose goal
pose_goal = geometry_msgs.msg.Pose()

#Setting the robot to an initial pose
pose_goal.orientation.w = 1.0
pose_goal.position.x = 0.7
pose_goal.position.y = 0.0
pose_goal.position.z = 0.1
move_group.set_pose_target(pose_goal)

#Plans and executes this pose
plan = move_group.go(wait=True)
move_group.stop()
move_group.clear_pose_targets()

#Calling the three functions to actually trace the letters
drawM()
drawE()
drawB()

