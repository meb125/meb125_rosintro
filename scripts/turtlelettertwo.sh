#! /bin/bash

rosservice call turtle1/teleport_absolute 1.5 2 1.57
rosservice call /clear
rosservice call /kill turtle2
rosservice call turtle1/teleport_absolute 1.5 6 5.5
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[2.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[0.0, 0.0, 0.0]' '[0.0, 0.0, 1.5]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[2.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[0.0, 0.0, 0.0]' '[0.0, 0.0, -2.25]'
rosservice call turtle1/teleport_relative 4.0 0.0

rosservice call /spawn 9 2 1.57 turtle2
rosservice call turtle2/set_pen 204 0 204 3 0
rosservice call turtle2/teleport_absolute 9 4.8 3.14
rosservice call turtle2/teleport_absolute 9 4.5 2.6
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[3.0, 0.0, 0.0]' '[0.0, 0.0, 2.4]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[2.5, 0.0, 0.0]' '[0.0, 0.0, 1.9]'


